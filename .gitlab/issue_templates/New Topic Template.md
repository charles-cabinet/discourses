# Overview

Topic

(A brief discussion of the topic on a single line)

Subject Category: (A list of comma seperated keywords indicating the subject category for the topic)

Summary

(A concise summary of what kind of discussion is expected)


# Preface

What is the context/motivation for this topic?

(What actually happens)

Starting Words

(A pitch for beginning the discussion of the topic)


# References

Relevant reference materials and/or resources.

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

Summary Text Location: (Location of summary of dicussion for this thread)

/label ~bug ~reproduced ~needs-investigation
/cc @project-manager
/assign @qa-tester